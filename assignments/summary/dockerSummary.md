 # Docker Overview
Docker is a container management service. The keywords of Docker are **develop**, **ship** and **run** anywhere. The whole idea of Docker is for developers to easily develop applications, ship them into containers which can then be deployed anywhere.

![Docker Overview](/extras/overview.jpeg)

## Features of Docker
1. Docker has the ability to reduce the size of development by providing a smaller footprint of the operating system via containers.
2. With containers, it becomes easier for teams across different units, such as development, QA and Operations to work seamlessly across applications.
3. We can deploy Docker containers anywhere, on any physical and virtual machines and even on the cloud.

## Docker Engine
  Docker Engine is an open source containerization technology for building and containerizing your applications. Docker Engine acts as a client-server application with:
 * A server with a long-running daemon process dockerd.
 * APIs which specify interfaces that programs can use to talk to and instruct the Docker daemon.
 * A command line interface (CLI) client docker.
  The CLI uses Docker APIs to control or interact with the Docker daemon through scripting or direct CLI commands. Many other Docker applications use the underlying API and CLI. The daemon creates and manage Docker objects, such as images, containers, networks, and volumes.

![](/extras/engine.png)

## Docker Terminologies
* ## Docker Image
  A Docker image is made up of a collection of files that bundle together all the essentials, such as installations, application code and dependencies, required to configure a fully operational container environment.
* ## Docker Container
  Docker Container is a standardized unit which can be created on the fly to deploy a particular application or environment. It could be an Ubuntu container, CentOs container, etc. to full-fill the requirement from an operating system point of view.
* ## Repository(repo)
  A collection of related Docker images, labeled with a tag that indicates the image version. Some repos contain multiple variants of a specific image, such as an image containing SDKs (heavier), an image containing only runtimes (lighter), etc. Those variants can be marked with tags. A single repo can contain platform variants, such as a Linux image and a Windows image.
* ## Docker Hub
  A public registry to upload images and work with them. Docker Hub provides Docker image hosting, public or private registries, build triggers and web hooks, and integration with GitHub and Bitbucket.
* ## Dockerfile
  A text file that contains instructions for building a Docker image. It's like a batch script, the first line states the base image to begin with and then follow the instructions to install required programs, copy files, and so on, until you get the working environment you need.

![Docker image](/extras/Arch.png)    

## Docker commands
* docker run – Runs a command in a new container.
* docker start – Starts one or more stopped containers
* docker stop – Stops one or more running containers
* docker build – Builds an image form a Docker file
* docker pull – Pulls an image or a repository from a registry
* docker push – Pushes an image or a repository to a registry
* docker export – Exports a container’s filesystem as a tar archive
* docker exec – Runs a command in a run-time container
* docker search – Searches the Docker Hub for images
* docker attach – Attaches to a running container
* docker commit – Creates a new image from a container’s changes

## How Does Docker Work?
Docker is implemented as a **client-server mode**; The Docker daemon runs on the Host and it is accessed via a socket connection from the client. The client may, but does not have to, be on the same machine as the daemon. The Docker CLI client works the same way as any other client but it is usually connected through a Unix domain socket instead of a TCP socket. The daemon receives commands from the client and manages the containers on the Host where it is running.

![Docker Working](/extras/working.png)

## Docker vs VM
* Docker is container based technology and containers are just user space of the operating system.At the low level, a container is just a set of       processes that are isolated from the rest of the system, running from a distinct image that provides all files necessary to support the processes. It is built for running applications.

* A Virtual Machine, on the other hand, is not based on container technology. They are made up of user space plus kernel space of an operating system. Under VMs, server hardware is virtualized. It shares hardware resource from the host.

![](/extras/download.png)