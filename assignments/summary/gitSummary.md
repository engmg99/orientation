# What is Git?
Git is a **version control system** which enables you to track changes to files. It is entirely file based itself. Using Git, you are able to revert files back to previous versions, restore deleted files, remove added files and even track down where a particular line of code was introduced.  

Git creates a .git folder (in the current folder) to store the details of the file system - this folder contains all the data required to track your files and is known as a repository, or repo.  

Git tracks file changes by the user creating a save point, or in Git terms a commit. Each commit takes a snapshot of the current file system rather than storing just the changes made since the last commit. This allows a commit ot be extracted and the whole history not required to rebuild the file system.

* replacement for BitKeeper to manage Linux kernel changes  
* a command line version control program 
* uses checksums to ensure data integrity 
* distributed version control (like BitKeeper) 
* cross-platform (including Windows!) 
* open source, free

![Overview](/extras/overviewgit.png)

## What is a repository? 
* “repo” = repository 
* usually used to organize a single project 
* repos can contain folders and files, images, videos, spreadsheets, and data sets – anything your project needs
![repo](/extras/gitrepos.gif)

## Git Workflow
Basiccaly the workflow attempts to insulate code bases and prevent problems before they happen. Some of the terms used in Git can seem a bit foreign at first and if you’re working in the command line it might be tricky to visualize the process. This simple step by step guide to a Git workflow aims to help give an introductory overview for structuring a group project.

![Workflow](/extras/workflow.png)

## Git Commands
1. cloning a repo:
```sh
$ git clone <link-to-repository>
```
2.	Creating a new branch:
```sh
$ git checkout master
$ git checkout -b <your-branch-name>
```

For modifying files in your working tree,

You selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.

```sh
$ git add .         # To add untracked files ( . adds all files) 
```

1.	You do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Local Git Repository.

```sh
$ git commit -sv   # Description about the commit
```

2.	You do a push, which takes the files as they are in the Local Git Repository and stores that snapshot permanently to your Remote Git Repository.

```sh
$ git push origin <branch-name>      # push changes into repository
```



## The Three Stages
* **Modified** - it means you have changes the contents of the file but not commited it to the local repository
* **Staged**- it mean that you have moves the changed file to the index to be commited in the local repository
* **Commit**- it means that you have stored your changed data safelyt in your local repository.

![stages](/extras/stages.png)










